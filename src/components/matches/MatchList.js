import React from 'react';
import { connect } from 'react-redux';

const MatchList = (props) => {
	const { oVoMatches } = props;

	return (
		<div>
			{oVoMatches &&
				oVoMatches.map((match, i) => {
					return (
						<div className="row" key={i}>
							<div className="col l6">Winner: {match.winner}</div>
							<div className="col l6">Loser: {match.loser}</div>
						</div>
					);
				})}
		</div>
	);
};

const mapStateToProps = (state) => {
	//console.log(state);
	const oVoMatches = state.match.oVoMatches;
	return {
		oVoMatches
	};
};

export default connect(mapStateToProps)(MatchList);
