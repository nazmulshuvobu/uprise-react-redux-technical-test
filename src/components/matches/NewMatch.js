import React, { Component } from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';

import { newMatchAction, updateOvO } from '../../store/actions/newMatchAction';

class NewMatch extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id: 0,
			team1: [],
			team2: [],
			winner: [],
			loser: [],
			date: new Date()
		};
	}

	setId = () => {
		this.setState({
			id: Math.random()
		});
	};

	playerNames = (players) => {
		let playerNameList = [];
		//console.log(players);
		players.forEach((player) => {
			playerNameList.push({
				value: [ player.name ],
				label: [ player.name ]
			});
		});
		return playerNameList;
	};

	handleTeam1 = (e) => {
		this.setState({
			team1: e.value
		});
	};

	handleTeam2 = (e) => {
		this.setState({
			team2: e.value
		});
	};
	handleWinner = (e) => {
		const team1 = this.state.team1;
		const team2 = this.state.team2;
		if (e.label === 'Team 1') {
			this.setState({
				winner: team1,
				loser: team2
			});
		} else {
			this.setState({
				winner: team2,
				loser: team1
			});
		}
	};

	OvOCounter = (name, oVoMatches) => {
		let total = 0;
		let wins = 0;
		let winPercentage = '';
		{
			oVoMatches &&
				oVoMatches.forEach((match) => {
					//console.log(name, match.team1);
					if (name === match.team1 || name === match.team2) {
						total += 1;
					}
					if (name === match.winner) {
						wins += 1;
					}
				});
		}
		if (total) {
			winPercentage = (wins * 100 / total).toFixed(2).toString() + '%';
		} else {
			winPercentage = '0.00%';
		}
		//console.log(winPercentage);
		return winPercentage;
	};

	handleSubmit = (e) => {
		e.preventDefault();
		if (this.state.team1 === '' || this.state.team2 === '' || this.state.team1 === this.state.team2) {
			alert('Team names are not valid');
		} else {
			this.setId();
			this.props.newMatchAction(this.state);
			this.props.updateOvO(this.OvOCounter(this.state.winner, this.props.oVoMatches));
		}
		//console.log(this.state);
	};

	render() {
		//console.log(this.props);
		const { players } = this.props;
		const playerNameList = this.playerNames(players);
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="row">
					<Select
						className="col l6 s6"
						options={playerNameList}
						onChange={this.handleTeam1}
						placeholder="Team 1"
					/>
					<Select
						className="col l6 s6"
						options={playerNameList}
						onChange={this.handleTeam2}
						placeholder="Team 2"
					/>
				</div>
				<div className="row">
					<Select
						className="col l12 s8"
						options={[ { label: 'Team 1' }, { label: 'Team 2' } ]}
						onChange={this.handleWinner}
						placeholder="Winner"
					/>
				</div>
				<div className="row">
					<button className="btn col">Add Match</button>
				</div>
			</form>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		players: state.player.players,
		oVoMatches: state.match.oVoMatches
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		newMatchAction: (match) => dispatch(newMatchAction(match)),
		updateOvO: (percentage) => dispatch(updateOvO(percentage))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(NewMatch);
