import React from 'react';
import { connect } from 'react-redux';

const Dashboard = (props) => {
	const { players } = props;
	//console.log(players);
	return (
		<div>
			{players &&
				players.map((player, i) => {
					return (
						<div className="row" key={i}>
							<div className="col l1">{player.id}</div>
							<div className="col l2">{player.name}</div>
							<div className="col l9">{player.winPercentage}</div>
						</div>
					);
				})}
		</div>
	);
};

const mapStateToProps = (state) => {
	//console.log(state);
	const players = state.player.players;
	return {
		players
	};
};

export default connect(mapStateToProps)(Dashboard);
