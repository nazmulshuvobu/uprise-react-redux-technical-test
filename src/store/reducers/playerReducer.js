const initState = {
	players: [
		{
			id: 1,
			name: 'John',
			winPercentage: '100.00%'
		},
		{
			id: 2,
			name: 'Andrew',
			winPercentage: '0.00%'
		},
		{
			id: 3,
			name: 'Ricky',
			winPercentage: '100.00%'
		},
		{
			id: 4,
			name: 'Clark',
			winPercentage: '0.00%'
		}
	]
};

const playerReducer = (state = initState, action) => {
	switch (action.type) {
		case 'UPDATE_OVO':
			console.log(action.percentage);
		// return {
		// 	[state.players.winPercentage]: action.percentage
		// };

		default:
			return state;
	}
};

export default playerReducer;
