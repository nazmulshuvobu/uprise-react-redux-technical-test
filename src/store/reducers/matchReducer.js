const initState = {
	oVoMatches: [
		{
			id: 1,
			team1: [ 'John' ],
			team2: [ 'Andrew' ],
			winner: [ 'John' ],
			loser: [ 'Andrew' ],
			date: new Date()
		},
		{
			id: 2,
			team1: [ 'Ricky' ],
			team2: [ 'Clark' ],
			winner: [ 'Ricky' ],
			loser: [ 'Clark' ],
			date: new Date()
		}
	]
};

const matchReducer = (state = initState, action) => {
	//console.log(action);
	switch (action.type) {
		case 'ADD_MATCH':
			return {
				oVoMatches: [ action.match, ...state.oVoMatches ]
			};

		default:
			return state;
	}
};

export default matchReducer;
