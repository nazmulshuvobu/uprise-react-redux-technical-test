import { combineReducers } from 'redux';

import playerReducer from './playerReducer';
import matchReducer from './matchReducer';

const rootReducer = combineReducers({
	player: playerReducer,
	match: matchReducer
});

export default rootReducer;
