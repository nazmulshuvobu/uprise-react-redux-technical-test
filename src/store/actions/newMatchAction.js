export const newMatchAction = (match) => {
	return {
		type: 'ADD_MATCH',
		match
	};
};

export const updateOvO = (percentage) => {
	return {
		type: 'UPDATE_OVO',
		percentage
	};
};
