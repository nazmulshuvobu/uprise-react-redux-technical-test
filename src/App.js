import React, { Component } from 'react';

import './App.css';
import Dashboard from './components/dashboard/Dashboard';
import MatchList from './components/matches/MatchList';
import NewMatch from './components/matches/NewMatch';

class App extends Component {
	render() {
		return (
			<div>
				<h1>Uprise Foosball Ranking System</h1>
				<Dashboard />
				<MatchList />
				<NewMatch />
			</div>
		);
	}
}

export default App;
